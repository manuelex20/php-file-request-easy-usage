<?php

class Files {
    #TODO: description

    public function __construct() {
        
    }

    public function getCount(): int {
        return count($_FILES);
    }

    public function getFile(string $name) {


        return new Archivo($name);
    }

}

class Archivo {

    private $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    private function validateFile() {
        switch ($_FILES[$this->name]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }
    }

    public function getName() {
        $this->validateFile();
        $param = strtolower(str_replace("get", "", __FUNCTION__));
        return $_FILES[$this->name][$param];
        //return $_FILES[$this->name]['name'];
    }

    public function getType() {
        $this->validateFile();
        $param = strtolower(str_replace("get", "", __FUNCTION__));
        return $_FILES[$this->name][$param];
        //return $_FILES[$this->name]['type'];
    }

    public function getTMP_NAME() {
        $this->validateFile();
        $param = strtolower(str_replace("get", "", __FUNCTION__));
        return $_FILES[$this->name][$param];
        //return $_FILES[$this->name]['tmp_name'];
    }

    public function getError() {
        $this->validateFile();
        $param = strtolower(str_replace("get", "", __FUNCTION__));
        return $_FILES[$this->name][$param];
        //return $_FILES[$this->name]['error'];
    }

    public function getSize() {
        $this->validateFile();
        $param = strtolower(str_replace("get", "", __FUNCTION__));
        return $_FILES[$this->name][$param];
        //return $_FILES[$this->name]['size'];
    }

    public function isExistentFile(): bool {
        return $_FILES[$this->name]['error'] == UPLOAD_ERR_OK ? true : false;
    }

    public function copyToServer(string $path, bool $override = false) {
        $this->validateFile();

        if (file_exists($path)) {
            if ($override) {
                return move_uploaded_file($this->getTMP_NAME(), $path);
            } else {
                return false;
            }
        }
        return move_uploaded_file($this->getTMP_NAME(), $path);
    }

}
